# openfoamの環境構築

2023/09/15  
M.TANAKA  

[TOC]

## 1. Linux環境の用意

OpenFOAMは基本的にはLinux環境で動作するため，まずLinux環境を用意する．

### 1.1 ネイティブLinuxの場合

- 更地になって良いコンピュータを用意する．

- isoファイルの入手．ディストリビューションはお好みで
    - 自分はパッケージが豊富で，プロキシ設定が1ファイルで済む[openSUSE Leap](https://get.opensuse.org/leap/)を使っている．デスクトップ環境がKDE plasmaなのでWindowsと同じように操作できる．
    - プロキシ設定の面倒くささが無視できるなら，Ubuntu系列のLTS版がメジャーで無難．Windowsからなら[Kubuntu](https://kubuntu.org/getkubuntu/)が違和感なく使える．

- USBにisoイメージを焼く．イメージライターは色々あるが[balenaEtcher](https://etcher.balena.io/)を使っている．Rufusと違ってWindowsにもLinuxにも対応しているので面倒がない．

- 用意したコンピュータにインストール．大筋は下記を参考に，色々ググればやり方はわかるはず．
    - [Ubuntu 20.04 LTS インストール方法（外付けドライブ用）](https://qiita.com/koba-jon/items/019a3b4eac4f60ca89c9)


### 1.2 Windowsの場合

WSL2を用いる．Windows 11 なら，PowerShellに簡単なコマンドを入力するだけで準備が完了する．

- [Windows 11でWSL2がさらに進化、1コマンドで導入可能に](https://xtech.nikkei.com/atcl/nxt/column/18/01863/112600005/)

10の場合，ちょっと面倒．

- [Windows 10でLinuxを使う(WSL2)](https://qiita.com/whim0321/items/ed76b490daaec152dc69)

### 1.3 プロキシ設定

openSUSEなら，下記のようにして設定できる．具体的なIPアドレスやポート番号は組織によるので要確認．
- [SUSE Linux Enterprise : How to setup a Proxy manually](https://www.suse.com/ja-jp/support/kb/doc/?id=000017441)

Ubuntuは，ググってください．


## 2. OpenFOAMのインストール

OpenFOAMには大きく2つの派閥がある．

- [The OpenFOAM Foundation](https://openfoam.org/)
- [OpenCFD Ltd](https://openfoam.com/)

自分はOpenCFD版を使っているので，そちらを説明する．下記ページに，Ubuntu，SUSEそれぞれのインストール方法が書いてある．

- https://develop.openfoam.com/Development/openfoam/-/wikis/precompiled

## 3. ParaViewのインストール

可視化にはParaViewが必要なので一緒にインストールする．

### 3.1 Windowsの場合

コマンドプロンプトにて下記コマンドを実行すればよい．（デフォルトではログイン中のユーザディレクトリにインストールされる．全ユーザが使えるように`--scope=machine`オプションをつけている．これによりインストール先が`C:\Program Files\ParaView X.XX.X`になる．）途中，コンピュータに変更を与えて良いか聞かれるのでYESとする．

```powershell
winget install Kitware.ParaView --scope=machine
```

### 3.2 Linuxの場合

openSUSE Leap15.5なら，

```bash
sudo zypper addrepo https://download.opensuse.org/repositories/science/15.5/science.repo
sudo zypper refresh
sudo zypper install paraview
```

- 参考：
    - https://software.opensuse.org/download/package?project=science&package=paraview


Ubuntuなら，

```bash
sudo apt install -y paraview
```
